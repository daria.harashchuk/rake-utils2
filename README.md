## Update Rake-Utils (var2)
The idea is that **step 1** allows you to work freely with ES6 syntax, **step2** (optional) contains types for better experience in TS projects.
### Step 1
* change CommonJS to ES6 syntax (require -> import/export)
* add `"type": "module"` in parent package.json

### Step 2 (optional)
#### Add type declarations:
* add JsDoc
* `npm install typescript --save-dev`
* Add a tsconfig.json to configure TypeScript
* Run the TypeScript compiler to generate the corresponding d.ts files for JS files `npx tsc`
#### Usage
* don't forget maintaining JsDoc
* after changes in project regenerate type declaration files
> MORE INFO https://www.typescriptlang.org/docs/handbook/declaration-files/dts-from-js.html


