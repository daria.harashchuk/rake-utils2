/**
 *
 * @param {Object} param0
 * @param {Object} param0.pack
 * @param {Object} param0.pack.message
 * @param {string} param0.pack.message.text
 * @param {string} param0.pack.message.senderLabel
 * @param {Object} param0.pack.message.quotedMessage
 * @param {string} param0.pack.message.type
 * @param {Object} param0.cacheData
 * @param {Object} param0.cacheData.messageType Enumerator (endSession)
 * @returns
 */
export function convertText({ pack, cacheData }: {
    pack: {
        message: {
            text: string;
            senderLabel: string;
            quotedMessage: any;
            type: string;
        };
    };
    cacheData: {
        messageType: any;
    };
}): Promise<string>;
//# sourceMappingURL=convert-text.d.ts.map