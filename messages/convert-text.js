import removeMarkdown from 'markdown-to-text';
import moment from 'moment';

/**
 *
 * @param {Object} param0
 * @param {Object} param0.pack
 * @param {Object} param0.pack.message
 * @param {string} param0.pack.message.text
 * @param {string} param0.pack.message.senderLabel
 * @param {Object} param0.pack.message.quotedMessage
 * @param {string} param0.pack.message.type
 * @param {Object} param0.cacheData
 * @param {Object} param0.cacheData.messageType Enumerator (endSession)
 * @returns
 */
const convertText = async ({ pack, cacheData = {} }) => {
  const {
    message: {
      text = '', senderLabel = '', quotedMessage, type,
    },
  } = pack;
  // This is enum, must be hosted on rake-utils
  const { messageType } = cacheData;

  // TODO check in DB if text is markdown
  // use next line of code only if text is markdown
  const normalizedText = removeMarkdown(text); //ERROR: TypeError: removeMarkdown is not a function

  if (!quotedMessage) {
    return type === messageType.endSession ? normalizedText : `${senderLabel}: ${normalizedText}`;
  }
  /**
   * Hard code of limitations for quotedMessage, must be put to DB
   */
  const LENGTH_LIMIT = 40; // 11 first ... 11 last

  let {
    sentAt,
    text: quotedMessageText,
    senderLabel: quotedMessageSender,
  } = quotedMessage;

  quotedMessageText = quotedMessageText?.length > LENGTH_LIMIT
    ? `"${(quotedMessageText || '').substr(0, LENGTH_LIMIT / 2)}...${(quotedMessageText || '')
      .substr(quotedMessageText.length - LENGTH_LIMIT / 2)}"`
    : quotedMessage.text || '';
  return `${senderLabel}:\nQuoted Message | "${quotedMessageText}" | ${quotedMessageSender} - ${moment(sentAt).format('ll')}\n---\n${text}`;
};

export { convertText }
